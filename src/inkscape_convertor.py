import os,re,fs,shutil,string
import ttk_output

from convertor import Convertor
from post_script_helper import PostScriptHelper

#------------------------------------------------------------------
class InkscapeConvertor( Convertor, PostScriptHelper ):
    def __init__(self,options):
        Convertor.__init__(self,options)
        self.options = options

    def run(self):
        for file_name in os.listdir( self.options.src_dir ):
            (source_eps,target_eps) = self.checkIfConversionNeeded(file_name,r"_ink.eps\b","_ink.eps")
            if target_eps:
                self.convert(source_eps,target_eps)

    def createCharacterMap(self,char_chain):
        self.char_map_ = []
        for i in range(len(char_chain)/2):
            self.char_map_.append(char_chain[i*2] + char_chain[i*2+1])

    def convert(self,source_eps,target_eps):
        source_eps_file_1 = file(source_eps,'r')
        source_eps_file_2 = file(source_eps,'r')
        target_eps_file = file(target_eps,'w')
        target_eps_content = ""
        font_content = ""
        for line in source_eps_file_1:
            match = re.search(r"\[<(?P<char_chain>[\da-f<>-]*)>\]TJ",line)
            if( match ):
                char_chain = match.group("char_chain").replace('>-1<','').replace('>1<','')
                self.createCharacterMap(char_chain)
                break

        line = source_eps_file_2.readline()
        while( line ):
            if( line == "BT\n" ):
                line = source_eps_file_2.readline()
                values = line.split(' ')
                font_content += values[4]+" "+values[5]+" m\n"
                line = source_eps_file_2.readline()
                line = source_eps_file_2.readline()
                match = re.search(r"<(?P<first_num>[\da-fA-F][\da-fA-F])(?P<second_num>[\da-fA-F][\da-fA-F])>Tj",line)
                if( match ):
                    first_char = chr(self.char_map_.index(match.group("first_num")) + 97)
                    #first_char = chr(int(match.group("first_num"),16) + 96)
                    #print match.group("first_num") + " | " + first_char
                    second_char = chr(self.char_map_.index(match.group("second_num")) + 97)
                    #second_char = chr(int(match.group("second_num"),16) + 96)
                    #print match.group("second_num") + " | " + second_char
                    font_content += line.replace("<"+match.group("first_num")+match.group("second_num")+">Tj", \
                                    self.epsTextWriterInk(first_char+second_char) )
                    # font_content += self.epsTextWriterInk(first_char+second_char) + "\n"
                line = source_eps_file_2.readline()
                line = source_eps_file_2.readline()
                continue

            if( line == "Q Q\n" ):
                target_eps_content += font_content

            target_eps_content += line
            line = source_eps_file_2.readline()

        target_eps_file.write( target_eps_content )
