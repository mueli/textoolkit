import os, sys, random, stat
import ttk_output

#------------------------------------------------------------------
def findPath(path):
    if path == "":
        return ""
    
    if path[0] != os.sep:
        full_path = os.path.abspath(path)
        if os.path.exists( full_path ):
            return full_path
    else:
        if os.path.exists( path ):
            return path

    print ttk_output.red("Error: ") + ttk_output.green(path) + " is not a valid path"
    sys.exit(1)

#------------------------------------------------------------------
def checkForFile(file_path):
    if file_path[0] != os.sep:
        full_path = os.path.abspath(file_path)
        if os.path.exists( full_path ):
            return full_path
    else:
        if os.path.exists( file_path ):
            return file_path

    print ttk_output.red("Error: ") + ttk_output.green(file_path) + " is not a valid file"
    sys.exit(1);

#------------------------------------------------------------------
class TempDir:
    def __init__(self,src_path):
        self.temp_dir = os.path.join(src_path, "img-temp" + str(random.randint(111111,999999)))
        os.mkdir(self.temp_dir)

    def getTempDir(self):
        return self.temp_dir

    def __del__(self):
        if self.temp_dir[-1] == os.sep: self.temp_dir = self.temp_dir[:-1]
        files = os.listdir(self.temp_dir)
        for file in files:
            if file == '.' or file == '..': continue
            path = self.temp_dir + os.sep + file
            if os.path.isdir(path):
                remdir(path)
            else:
                os.unlink(path)
        os.rmdir(self.temp_dir)

#------------------------------------------------------------------
def checkForRebuild( source_file , target_file ):
    source_mtime = os.stat( source_file ) [ stat.ST_MTIME ]
    if not os.path.exists( target_file ):
        return True
    target_mtime = os.stat( target_file ) [ stat.ST_MTIME ]
    if source_mtime > target_mtime:
        return True
    else:
        return False
