#------------------------------------------------------------------
class Version :
    version = "0.20"

    #------------------------------------------------------------------
    def __init__(self):
        # FIXME: should we set version in external file?
        pass

    #------------------------------------------------------------------
    def __call__(self, option, opt, value, parser) :
        print "Version "+self.version
        sys.exit()

    #------------------------------------------------------------------
    def __str__(self):
        return( self.version )
