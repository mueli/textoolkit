import os, fs

import ttk_output

#------------------------------------------------------------------
class PreambleExtractor:
    "provides the preamble for the *_img.tex files"

    def __init__(self,options):
        self.options = options
        self.main_file = file( options.main_file , 'r' )
        self.preamble = ""

    def get(self):
        for line in self.main_file:
            if not ( line.lstrip().startswith('%') ):
                # in the case of beamer presentation we have to use a default documentclass
                if ( line.find("{beamer}") != -1 ):
                    if self.options.verbose:
                        print ttk_output.green( "Found Beamer .tex file - using scrartcl as default documentclass" )
                    self.preamble += '\\documentclass[10pt]{scrartcl}\n'
                    self.preamble += '\\renewcommand{\\familydefault}{\\sfdefault}\n'
                    self.preamble += '\\usepackage[cm]{sfmath}\n'
                    break

                if ( line.find("begin{document}") == -1 ):
                    if ( ( line.find("include{") == -1 ) and ( line.find("input{") == -1 ) ) :
                        self.preamble = self.preamble + line
                    else:
                        if ( line.find("include{") != -1 ):
                            include_name = os.path.join(os.path.dirname(self.options.main_file), line.lstrip(" \\include{").rstrip("\n} #") + ".tex")
                        if ( line.find("input{") != -1 ):
                            include_name = os.path.join(os.path.dirname(self.options.mail_file), line.lstrip(" \\input{").rstrip("\n} #") + ".tex")
                        include_name = fs.checkForFile(include_name)
                        if self.options.verbose:
                            print "Including the file '" + include_name + "' into preamble"
                        include_file = file( include_name , 'r' )
                        for include_line in include_file:
                            self.preamble = self.preamble + include_line
                        include_file.close()
                else:
                    break

        self.addDefaultPreambleStuff()
        return self.preamble
    
    def addDefaultPreambleStuff(self):
        self.preamble += "\\usepackage{graphicx}\n"
        self.preamble += "\\usepackage{psfrag}\n"
        self.preamble += "\\usepackage{pstricks}\n"
        self.preamble += "\\thispagestyle{empty}\n"
        self.preamble += "\\newsavebox{\pict}\n"
        
    def __del__(self):
        self.main_file.close()
