
#------------------------------------------------------------------
class PostScriptHelper:
    argument_start_char = ['[', '\\' , '(' ]

    def tokenIsArgument(self, token, argument_stack):
        if token.isdigit():
            argument_stack.append( int(token) )
            return True
        elif token[0] in self.argument_start_char:
            argument_stack.append( token )
            return True
        else:
            try:
                float_arg = float(token)
            except ValueError:
                return False
            else:
                argument_stack.append( float_arg )
                return True

    # FIXME: merge the following methods!

    def epsTextWriter( self, coordinates , text_counter ):
        height = 20
        return "/Times-Roman findfont "+str(height)+" scalefont setfont " + \
            str(coordinates[0]) + " " + \
            str(coordinates[1]) + " moveto gsave " + \
            str(coordinates[5]) + " rotate (text_" + str(text_counter) + ") show grestore"

    def epsTextWriterInk( self, text ):
        height = 20
        return "/Times-Roman findfont "+str(height)+" scalefont setfont (" + str(text) + ") show grestore"
    
    def epsTextWriterQcad( self, coordinates , text_counter ):
        height = 20
        return "/Times-Roman findfont "+str(height)+" scalefont setfont " + \
            str(coordinates[0]) + " " + \
            str(coordinates[1]) + " moveto gsave 1 -1 scale " + \
            str(coordinates[5]) + " rotate (text_" + str(text_counter) + ") show grestore"
