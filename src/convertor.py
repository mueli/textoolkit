import os,re,fs,shutil
import ttk_output

#------------------------------------------------------------------
class Convertor:
    def __init__(self,options):
        self.options = options

    def checkIfConversionNeeded(self, file_name, reg_ex, file_ending):
        if re.search(reg_ex,file_name):
            if self.options.verbose:
                print ttk_output.green( "Converting '" + os.path.join(self.options.src_dir, file_name) + "' ..." )
                
            source_eps = os.path.join( self.options.src_dir, file_name )
            target_eps = os.path.join( self.options.src_dir, file_name.replace(file_ending,"_img.eps" ) )

            if fs.checkForRebuild( source_eps , target_eps):
                if self.options.verbose:
                    print ttk_output.red( "Writing" ) + ttk_output.green( "    '" + target_eps + "' ..." )
                return (source_eps,target_eps)
            else:
                if self.options.verbose:
                    print ttk_output.green( "   -> No conversion needed - already up-to-date" )   
                return ('','')
        else:
            return ('','')            
