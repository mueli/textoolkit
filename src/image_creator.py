import os, re, subprocess

from preamble_extractor import PreambleExtractor
import fs, ttk_output

#------------------------------------------------------------------
class ImageCreator:
    def __init__(self, options):
        self.options = options

    def run(self):
        self.preamble_extractor = PreambleExtractor( self.options )
        self.preamble = self.preamble_extractor.get()

        # FIXMEE: should we enable single file conversion?
        #if self.config.config_set["src_image_file"]:
        #    self.singleFileConvert(self.config.config_set["src_image_file"],my_temp)
        #else:

        for file_name in os.listdir( self.options.src_dir ):
            if re.search(r'_img.eps\b',file_name):
                if self.options.verbose:
                    print ttk_output.green( "Converting '" + os.path.join(self.options.src_dir, file_name) + "' ..." )

                self.source_eps = os.path.join( self.options.src_dir, file_name )
                self.source_tex = os.path.join( self.options.src_dir, file_name.replace("eps","tex") )
                fs.checkForFile( self.source_tex )
                self.target_eps = os.path.join( self.options.dst_dir, file_name.replace("_img.eps",".eps"))
                
                # FIXMEE: should we enable single file conversion?
                # if self.config.action_set["new"] or 
                if fs.checkForRebuild( self.source_eps , self.target_eps) or fs.checkForRebuild( self.source_tex , self.target_eps ):
                    if self.options.verbose:
                        print ttk_output.red( "Writing" ) + ttk_output.green( "    '" + self.target_eps + "' ..." )
                    self.convert()
                else:
                    if self.options.verbose:
                        print ttk_output.green( "   -> No conversion needed - already up-to-date" )                    

    #------------------------------------------------------------------
    def convert(self):
        temp = fs.TempDir(self.options.src_dir)        
        image_tex_file = file( os.path.join( temp.getTempDir(), "image.tex") , 'w' )
        image_options = ""
        image_psfrag = ""
        source_tex_file = file(self.source_tex,'r')
        for line in source_tex_file:
            if ( line.find("%opt:") != -1 ):
                image_options = "[" + line.replace('%opt:','').strip(' \n') + "]"
            if ( line.find("\\psfrag") != -1 ):
                image_psfrag += line.strip(' ')
        source_tex_file.close()
        tex_document = ""
        tex_document += "\\begin{document}\n"
        tex_document += "\\begin{lrbox}{\\pict}\n"
        tex_document += image_psfrag
        tex_document += "\\includegraphics"+image_options+"{"+ os.path.join( temp.getTempDir(),"include.eps") +"}\n"
        tex_document += "\\end{lrbox}\n"
        tex_document += "\\special{papersize=\\the\\wd\\pict,\\the\\ht\\pict}\n"
        tex_document += "\\usebox{\\pict}\n"
        tex_document += "\\end{document}"
        image_tex_file.write( self.preamble )
        image_tex_file.write( tex_document )
        image_tex_file.close()

        if self.options.debug:
            print ttk_output.red( "Starting epstool ..." )
        epstool_proc = subprocess.Popen( \
            ["epstool","--copy","-b",self.source_eps,os.path.join( temp.getTempDir(),"include.eps" )], \
                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        s = epstool_proc.communicate()
        if self.options.debug:
            print ttk_output.red("epstool output:")
            print s[0]
            print s[1]

        if self.options.debug:
            print ttk_output.red( "Starting LaTeX ..." )
        latex_proc = subprocess.Popen( \
            ["latex" , "-output-directory="+temp.getTempDir(), os.path.join( temp.getTempDir(),"image.tex" ) ], \
                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        s = latex_proc.communicate()
        if self.options.debug:
            print ttk_output.red("latex output:")
            print s[0]
            print s[1]

        if self.options.debug:
            print ttk_output.red( "Starting dvips ..." )
        dvips_proc = subprocess.Popen( \
            ["dvips","-R0","-E","-P","pdf",os.path.join(temp.getTempDir(),"image.dvi"),"-o",os.path.join(temp.getTempDir(),"image.ps")], \
                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        s = dvips_proc.communicate()
        if self.options.debug:
            print ttk_output.red("dvips output:")
            print s[0]
            print s[1]
        
        if self.options.debug:
            print ttk_output.red( "Starting epstool ..." )
        epstool_proc = subprocess.Popen( \
            ["epstool","--copy","-b",os.path.join(temp.getTempDir(), "image.ps"),self.target_eps], \
                stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        s = epstool_proc.communicate()
        if self.options.debug:
            print ttk_output.red("epstool output:")
            print s[0]
            print s[1]

        if self.options.debug:
            print ttk_output.red( "Starting epstopdf ..." )
        epstopdf_proc = subprocess.Popen( \
            ["epstopdf "+self.target_eps+" --outfile="+self.target_eps.replace("eps","pdf")], \
                stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        s = epstopdf_proc.communicate()
        if self.options.debug:
            print ttk_output.red("epstopdf output:")
            print s[0]
            print s[1]

        image_tex_file.close()
