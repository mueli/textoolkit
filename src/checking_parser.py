import os, fs

import ttk_output

#------------------------------------------------------------------
class CheckingParser:
    def __init__(self, parser):
        self.parser = parser

    def getOptions(self):
        (options, args) = self.parser.parse_args()

        if not len(args):
            self.parser.error("You must provide a MAIN_FILE")

        options.main_file = fs.checkForFile( args[0] )
        if options.verbose:
            print ttk_output.green("MAIN_FILE = ") + options.main_file

        options.base_dir = os.path.dirname(options.main_file)
        if options.verbose:
            print ttk_output.green("BASE_DIR  = ") + options.base_dir
        
        if not options.src_dir:
            options.src_dir = os.path.join(options.base_dir,"src_images")
        options.src_dir = fs.findPath(options.src_dir)
        if options.verbose:
            print ttk_output.green("SRC_DIR   = ") + options.src_dir

        if not options.dst_dir:
            options.dst_dir = os.path.join(options.base_dir,"images")
        options.dst_dir = fs.findPath(options.dst_dir)
        if options.verbose:
            print ttk_output.green("DST_DIR   = ") + options.dst_dir

        return options
