import os,re,fs,shutil,string,subprocess
import ttk_output
from math import sqrt,atan2,pi

from convertor import Convertor
from post_script_helper import PostScriptHelper

#------------------------------------------------------------------
class AcadConvertor(Convertor,PostScriptHelper):
    text_color_code = [1,0,1]
    setcolor = ["setrgbcolor", "setgray"]
    moveto = "moveto"
    lineto = "lineto"
    stroke = "stroke"
    showpage = "showpage"

    def __init__(self,options):
        Convertor.__init__(self,options)
        self.options = options

    def run(self):
        for file_name in os.listdir( self.options.src_dir ):
            (source_eps,target_eps) = self.checkIfConversionNeeded(file_name,r"_acad.eps\b","_acad.eps")
            if target_eps:
                self.convert(source_eps,target_eps)

    def calcLineLength(self, coordinates):
        coordinates.append( sqrt( (coordinates[2]-coordinates[0])**2 + (coordinates[3]-coordinates[1])**2 ) )
        
    def calcAngle(self, coordinates):
        coordinates.append( atan2( (int(coordinates[3]) - int(coordinates[1]) ) , \
                                       (int(coordinates[2]) - int(coordinates[0])) ) * 180. / pi - 90 )

    def writeEPSText(self,target_eps_content,all_text_coordinates):
        for coordinates in all_text_coordinates:
            self.calcLineLength(coordinates)
            self.calcAngle(coordinates)

        sorted_coordinates = sorted( all_text_coordinates , key=lambda coordinate: coordinate[4] )
        for index, coordinates in enumerate( sorted_coordinates ):
            target_eps_content += self.epsTextWriter( coordinates, index ) + "\n"

    def convert(self,source_eps,target_eps):
        source_eps_file = file(source_eps,'r')
        target_eps_file = file(target_eps,'w')

        target_eps_content = ""

        # we push all arguments on this stack - we clear it after the command was executed
        argument_stack = []
        single_text_coordinates = []
        all_text_coordinates = []

        global_state = "BEFORE"
        text_line_state = "BEFORE"

        for line in source_eps_file:
            line_tokens = line.split()
            if len(line_tokens) == 0:
                continue

            for token in line_tokens:
                if token[0] == '%':
                    target_eps_content += line.rstrip()
                    break
                
                # is it an argument? If YES - we CONTINUE to next token!
                if self.tokenIsArgument( token , argument_stack ):
                    continue
            
                # print "EPS:  " + token + "(" + str(argument_stack) + ")"

                if global_state == "BEFORE":
                    if ( (token in self.setcolor) and (argument_stack == self.text_color_code) ):
                        global_state = "TEXTMODE"
                        # print "-> We entered TEXTMODE"
                    elif token == self.showpage:
                        self.writeEPSText( target_eps_content , all_text_coordinates )
                        target_eps_content += token + " "
                    else:
                        for argument in argument_stack:
                            target_eps_content += str(argument) + " "
                        target_eps_content += token + " "
                        
                elif global_state == "TEXTMODE":
                    if ( (token in self.setcolor) and not (argument_stack == self.text_color_code) ):
                        for argument in argument_stack:
                            target_eps_content += str(argument) + " "
                        target_eps_content += token + " "
                        global_state = "BEFORE"
                        # print "-> Left TEXTMODE"
                    else:
                        if text_line_state == "BEFORE":
                            if token == self.moveto:
                                single_text_coordinates.append(argument_stack[0])
                                single_text_coordinates.append(argument_stack[1])
                                text_line_state = "MOVETO"
                            else:
                                if token == self.showpage:
                                    self.writeEPSText( target_eps_content , all_text_coordinates )
                                    target_eps_content += token + " "
                        elif text_line_state == "MOVETO":
                            if token == self.lineto:
                                single_text_coordinates.append(argument_stack[0])
                                single_text_coordinates.append(argument_stack[1])
                                text_line_state = "LINETO"
                            else:
                                text_line_state = "BEFORE"
                        elif text_line_state == "LINETO":
                            if token == self.stroke:
                                print "We have to draw a text line! " + str(single_text_coordinates)
                                all_text_coordinates.append( single_text_coordinates )
                                single_text_coordinates = []
                            text_line_state = "BEFORE"
                del argument_stack[:]
            target_eps_content += '\n'

        target_eps_file.write( target_eps_content )
