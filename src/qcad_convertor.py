import os,re,fs,shutil,string,subprocess
import ttk_output
from math import sqrt,atan2,pi

from convertor import Convertor
from post_script_helper import PostScriptHelper

#------------------------------------------------------------------
class QcadConvertor(Convertor,PostScriptHelper):
    text_color_code = [255,0,255,1,1]
    setcolor = ["PE"]
    vertical_line = "VL"
    horizontal_line = "HL"
    diagonal_line = "DL"
    showpage = "QP"

    def __init__(self,options):
        Convertor.__init__(self,options)
        self.options = options

    def run(self):
        for file_name in os.listdir( self.options.src_dir ):
            (source_ps,target_eps) = self.checkIfConversionNeeded(file_name,r"_qcad.ps\b","_qcad.ps")
            if target_eps:
                temp_ps = os.path.join( target_eps.replace("_img.eps","_img.ps" ) )
                self.convert(source_ps,temp_ps)
                if self.options.debug:
                    print ttk_output.red( "Starting ps2eps ..." )
                ps2eps_proc = subprocess.Popen( \
                    ["ps2eps -f " + str(temp_ps) ], \
                        stderr=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
                s = ps2eps_proc.communicate()
                if self.options.debug:
                    print ttk_output.red("ps2eps output:")
                    print s[0]
                    print s[1]
                os.remove(temp_ps)

    def calcLineLength(self, coordinates):
        coordinates.append( sqrt( (coordinates[2]-coordinates[0])**2 + (coordinates[3]-coordinates[1])**2 ) )
        
    def calcAngle(self, coordinates):
        coordinates.append( atan2( -(int(coordinates[3]) - int(coordinates[1]) ) , \
                                       (int(coordinates[2]) - int(coordinates[0])) ) * 180. / pi - 90 )

    def writeEPSText(self,all_text_coordinates):
        for coordinates in all_text_coordinates:
            self.calcLineLength(coordinates)
            self.calcAngle(coordinates)

        eps_code = ""

        sorted_coordinates = sorted( all_text_coordinates , key=lambda coordinate: coordinate[4] )
        for index, coordinates in enumerate( sorted_coordinates ):
           eps_code += self.epsTextWriterQcad( coordinates, index ) + "\n"

        return eps_code

    def convert(self,source_ps,temp_ps):
        source_ps_file = file(source_ps,'r')
        temp_ps_file = file(temp_ps,'w')

        target_ps_content = ""

        # we push all arguments on this stack - we clear it after the command was executed
        argument_stack = []
        single_text_coordinates = []
        all_text_coordinates = []

        global_state = "BEFORE"
        text_line_state = "BEFORE"

        for line in source_ps_file:
            line_tokens = line.split()
            if len(line_tokens) == 0:
                continue

            for token in line_tokens:
                if token[0] == '%':
                    target_ps_content += line.rstrip()
                    break

                # is it an argument? If YES - we CONTINUE to next token!
                if self.tokenIsArgument( token , argument_stack ):
                    continue
                if token == 'B':
                    argument_stack.append( token )
                    continue

                # print "EPS:  " + token + "(" + str(argument_stack) + ")"

                if global_state == "BEFORE":
                    if ( (token in self.setcolor) and (argument_stack[2:] == self.text_color_code) ):
                        global_state = "TEXTMODE"
                        # print "-> We entered TEXTMODE"
                    elif token == self.showpage:
                        target_ps_content += self.writeEPSText( all_text_coordinates )
                        target_ps_content += token + " "
                    else:
                        #print " ------> undefined token " + token + " ------> printing argument stack" + str(argument_stack)
                        for argument in argument_stack:
                            target_ps_content += str(argument) + " "
                        target_ps_content += token + " "
                        
                elif global_state == "TEXTMODE":
                    if ( (token in self.setcolor) and not (argument_stack[2:] == self.text_color_code) ):
                        for argument in argument_stack:
                            target_ps_content += str(argument) + " "
                        target_ps_content += token + " "
                        global_state = "BEFORE"
                        # print "-> Left TEXTMODE "
                    else:
                        if token == self.vertical_line:
                            single_text_coordinates.append(argument_stack[0]) #x1
                            single_text_coordinates.append(argument_stack[2]) #y1
                            single_text_coordinates.append(argument_stack[0]) #x2
                            single_text_coordinates.append(argument_stack[1]) #y2
                            # print "We have to draw a horizontal text! " + str(single_text_coordinates)
                            all_text_coordinates.append( single_text_coordinates )
                            single_text_coordinates = []
                        elif token == self.horizontal_line:
                            single_text_coordinates.append(argument_stack[2]) #x1
                            single_text_coordinates.append(argument_stack[1]) #y1
                            single_text_coordinates.append(argument_stack[0]) #x2
                            single_text_coordinates.append(argument_stack[1]) #y2
                            # print "We have to draw a vertical text! " + str(single_text_coordinates)
                            all_text_coordinates.append( single_text_coordinates )
                            single_text_coordinates = []
                        elif token == self.diagonal_line:
                            single_text_coordinates.append(argument_stack[2]) #x1
                            single_text_coordinates.append(argument_stack[3]) #y1
                            single_text_coordinates.append(argument_stack[0]) #x2
                            single_text_coordinates.append(argument_stack[1]) #y2
                            # print "We have to draw a diagonal text! " + str(single_text_coordinates)
                            all_text_coordinates.append( single_text_coordinates )
                            single_text_coordinates = []
                        elif token == self.showpage:
                            # print "-> We reached end of document no more text to convert"
                            target_ps_content += self.writeEPSText( all_text_coordinates )
                            target_ps_content += token + " "

                del argument_stack[:]
            target_ps_content += '\n'
        temp_ps_file.write( target_ps_content )
