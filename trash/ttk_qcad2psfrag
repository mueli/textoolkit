#!/usr/bin/python -O
#####################################################################
# ttk_qcad2psfrag
version = "0.1beta"
#####################################################################
# Copyright (C) 2007 Michael Hammer <michael(at)derhammer.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# TeXToolKit
#

import os
import sys
import re
import math

from ttkOutput import green

import ttkUtils
import ttkHelp

tool_name = "qcad2psfrag"
options = [
    "--src_image_path",
    "--src_image_file",
    ]
standard_args = [
    "src_images",
    ""
    ]
actions= [
    "--new",
    "--verbose"
    ]

# order: options help strings, action help strings
help_text = [
    "specify the path to the *_acad.eps",
    "specify a single image file to convert",
    "also acts on unchanged files in the src_image_path",
    "acts in verbose mode"
    ]

#------------------------------------------------------------------
class Config:
    "holds all settings needed to convert"
    def __init__(self,my_options,my_actions):
        self.config_set = {}
        self.action_set = {}

        for pos,x in enumerate(options):
            if pos == 0:
                if x in my_options:
                    self.config_set["src_image_path"] = ttkUtils.findPath(my_options[x],os.getcwd())
                else:
                    self.config_set["src_image_path"] = ttkUtils.findPath(standard_args[pos],os.getcwd())

            elif pos == 1:
                suggested_path_list = ["src_images"]
                if x in my_options:
                    self.config_set["src_image_file"] = ttkUtils.findFile(my_options[x],os.getcwd(),suggested_path_list)
                else:
                    self.config_set["src_image_file"] = ttkUtils.findFile(standard_args[pos],os.getcwd(),suggested_path_list)

        for pos,x in enumerate(actions):
            if pos == 0:
                if x in my_actions:
                    self.action_set["new"] = True
                    if self.config_set["src_image_file"]:
                        print green("!!!Warning:")+ " --new not necessary if single src_image_file given!"
                else:
                    self.action_set["new"] = False
                
    def prettyPrint(self):
        for x in self.config_set:
            print "Option: " + bold(x) + " = " + green(self.config_set[x])

#------------------------------------------------------------------
def epsFontWriter(single_set,orientation,font_set_counter):
    height = 100
    font_statement = "/Helvetica findfont "+str(height)+" scalefont setfont "+str(single_set[0])+" "+str(single_set[1])+" moveto gsave 1 -1 scale "+str(single_set[2])+" rotate (text_" + str(font_set_counter) + ") "
    if orientation == 'c':
        font_statement += "dup stringwidth pop 2 div neg 0 rmoveto "
    if orientation == 'r':
        font_statement += "dup stringwidth pop neg 0 rmoveto "
    font_statement += "show grestore \n"
    return font_statement

#------------------------------------------------------------------
class Converter:
    def __init__(self,my_config):
        self.config = my_config

    def baseNameExtractor(self, src_image_file):
        dir_name,file_name = os.path.split(src_image_file)
        if re.search(r'_qcad.ps\b',file_name) :
            base_name = file_name.rstrip('qcad.ps').rstrip('_')
        else:
            base_name = file_name.rstrip('.ps')
        return base_name

    def singleFileConvert(self, src_image_file,my_temp):
        base_name = self.baseNameExtractor(src_image_file)

        font_set_counter = 0
        eps_document = ""
        font_cache = "1 0 B 1 1 PE\n"
        single_set = []
        font_active = ''
        for next_line in ttkUtils.LineFileContainer(src_image_file):
            if next_line == "QP\n":
                eps_document += font_cache
                eps_document += next_line
                continue
            if re.match(r'.*255 0 127 1 1 PE$',next_line):
                font_active = 'l'
                continue
            if re.match(r'.*127 255 127 1 1 PE$',next_line):
                font_active = 'c'
                continue
            if re.match(r'.*85 0 255 1 1 PE$',next_line):
                font_active = 'r'
                continue
            if font_active:
                if re.match(r'.*PE$',next_line):
                    if not (re.match(r'.*1 1 255 1 1 PE$',next_line) or 
                            re.match(r'.*1 255 1 1 1 PE$',next_line) or 
                            re.match(r'.*255 1 1 1 1 PE$',next_line)):
                        font_active = ''
                        eps_document += next_line
                        continue

                if next_line == "\n":
                    font_active = ''
                    continue

                words = next_line.split()
                coord = [0.,0.,0.]
                if len(words) == 4:
                    if words[3] == "VL" :
                        if words[2] > words[1]:
                            coord[2] = 0
                        else:
                            coord[2] = 180
                        coord[0] = words[0]
                        coord[1] = words[2]
                    elif words[3] == "HL" :
                        if words[2] > words[0]:
                            coord[2] = 90
                        else:
                            coord[2] = 270
                        coord[0] = words[2]
                        coord[1] = words[1]
                    else:
                        print "You have to draw a line with the apropriate colour!"
                        print "I've detected: "+words[3]
                        sys.exit(-1)
                elif len(words) == 5:
                    if words[4] == "DL":
                        coord[0] = words[2]
                        coord[1] = words[3]
                        coord[2] = math.atan2 ( (int(words[3]) - int(words[1]) ) , (int(words[0]) - int(words[2])) ) * 180. / math.pi - 90
                    else:
                        print "You have to draw a line with the apropriate colour!"
                        print "I've detected: "+words[4]
                        
                font_set_counter += 1
                single_set = []
                font_cache += epsFontWriter(coord,font_active,font_set_counter)
            else:
                eps_document += next_line

        target_image_file = file( my_temp.getTempDir() + os.sep + "image.ps", 'w')
        target_image_file.write( eps_document )
        target_image_file.close()
        destination_eps = self.config.config_set["src_image_path"] + os.sep + base_name + "_img.eps"
        ps2eps_message = os.system( "ps2eps -f "+my_temp.getTempDir()+os.sep+"image.ps" )
        epstool_message = os.system( "epstool --copy -b "+my_temp.getTempDir()+os.sep+"image.eps "+destination_eps)#+" --quiet" )
            
    def run(self):
        my_temp = ttkUtils.TempDir(self.config.config_set["src_image_path"])
        if self.config.config_set["src_image_file"]:
            self.singleFileConvert(self.config.config_set["src_image_file"],my_temp)
        else:
            for fileName in os.listdir( self.config.config_set["src_image_path"] ):
                if re.search(r'_qcad.ps\b',fileName):
                    source_eps = self.config.config_set["src_image_path"]+os.sep+fileName
                    target_eps = self.config.config_set["src_image_path"]+os.sep+self.baseNameExtractor(self.config.config_set["src_image_path"]+os.sep+fileName)+"_img.eps"
                    if self.config.action_set["new"] or ttkUtils.makeNew( source_eps , target_eps):
                        self.singleFileConvert(self.config.config_set["src_image_path"]+os.sep+fileName,my_temp)

#------------------------------------------------------------------
def main():
    tmp_cmd_line = sys.argv[1:]
    my_options, my_actions = ttkUtils.parseOptions(tmp_cmd_line,options,actions)

    my_config = Config(my_options,my_actions)

    if ttkHelp.version_str in my_actions:
        ttkHelp.version(tool_name,version)
        sys.exit(0)
        
    elif ttkHelp.help_str in my_actions:
        ttkHelp.help(tool_name,my_actions,options,actions,help_text)
        sys.exit(0)

    if actions[1] in my_actions:
        my_config.prettyPrint()

    my_converter = Converter(my_config)
    my_converter.run()

#==================================================================
if __name__ == "__main__":
    sys.exit(main());
