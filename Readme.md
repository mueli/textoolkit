# TeXToolKit: (ttk)

## Dependencies

At first make sure you have installed the following software:

 - A suitable LaTeX distribution
 - epstool
 - >=python-2.5

## The Idea

The idea behing TeXToolKit is to provide an infrastructure for
creating images containing LaTeX formulas (or other text).

### `\psfrag` usage

First the big goal is to use `\psfrag` to replace text inside the
graphics. `\psfrag` itself needs the text inside the `*.eps` files in
a parsable way. The only tool which is able to export such `*.eps`
files at the moment is `xfig`. What TeXToolKit does is to combine a

 - `src_images/${file}_img.eps` and a
 - `src_images/${file}_img.tex`

and combine and compile them to

 - images/${file}.eps and
 - images/${file}.pdf

which enables you to compile either with `latex` **OR/AND**
`pdflatex`.

### Enable different vector graphics software

Second some wrapper parts were written which convert `*.eps` files
exported from AutoCAD, QCad, Inkscape into a parsable format for
`\psfrag`.

## Usage

You should create a project structure for you LaTeX file similar to
the following:

	${cwd}
	|-- src_images/
	|   |-- a_figure_img.tex
	|   `-- a_figure_img.eps
	|-- images/
	`-- main.tex

What TexToolKit can do for you now is to create `a_figure.eps` and
`a_figure.pdf` inside the `images` directory. Of course you can choose
the directory names freely - but those names are the defaults.

To create the two files enter the following command line

	ttkCreateImages ${cwd}/main.tex

`ttkCreateImages` will parse the preamble of `main.tex` and create the
two image files in `images`.

To get a short help of all the options enter

	ttkCreateImages --help

### Format of `*_img.tex` file

The content of those files might look like the following

	% opt: <options for includegraphics>
	 \psfrag{}[][]{}

e.g.

	%opt: width=0.6\linewidth
	\psfrag{aa}[l][l]{$\frac{1}{2}$}
	\psfrag{ab}[l][l]{$\int\limits_0^{2 \pi} \cos(\varphi) {\rm d} \varphi$}
	\psfrag{ac}[l][l]{$\frac{\partial f(x,y)}{\partial x}$}

### Convert Inkscape `*.eps` files

There is currently (inkscape version 0.47-0.4?) a "bug" (or however
you might call it) inside cairo/inkscape relation. The `*.eps` files
exported from inkscape are not suitable for `\psfrag`. To use inkscape
as graphics program simply save your graphics inside `src_images` with
the name

	${file}_ink.eps
	${file}_img.tex

What you have to do is to "draw" a text line with all characters from
'a' to 's' somewhere inside the picture. Don't worry - textoolkit will
remove this line from the picture and will recalc the boundaries!

	abcdefghijklmnopqrs
